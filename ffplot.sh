#!/bin/bash

# defaults
verbose=0
mode=pts_size
preserve=0

function print_help_and_exit {
    cat <<EOF
Usage: `basename $0` [options]
  Plot video information, using ffprobe and gnuplot.

Options:
  -h, --help          print this help
  -v, --verbose       verbose output (default: ${verbose})
  -m | --mode mode    (default: ${mode})
       	      where mode: 
	      - pts_size: x=pkt_pts_time, y=pkt_size
	      - cpn_size: x=coded_picture_number, y=pkt_size
	      - cpn_pts:  x=coded_picture_number, y=pkt_pts_time
  -p | --preserve             preserve data (in data_dir)
  -d | --dir data_dir         dir with data retrieved with ffplot
  -i | --input input_media    file to analyse

Examples:
- `basename $0` -v -i video.mp4
- `basename $0` -v -m cpn_pts -i https://server.org/video.m3u8
EOF
    exit 1
}

MIN_ARGS=1
MAX_ARGS=8
if (( $# < $MIN_ARGS )) || (( $# > $MAX_ARGS ))
then
    print_help_and_exit
fi


# options
if ! params=$(getopt -o hvi:m:pd:i: --long help,verbose,input:,mode:,preserve,dir:,input: -n $0 -- "$@")
then
    # invalid option
    print_help_and_exit
fi
eval set -- ${params}

while true
do
    case "$1" in
        -h | --help ) print_help_and_exit;;
        -v | --verbose ) verbose=1; shift ;;
        -i | --input ) input_media="$2"; shift 2 ;;
	-d | --dir ) data_dir="$2"; shift 2 ;;
	-m | --mode ) mode="$2"; shift 2 ;;
	-p | --preserve ) preserve=1; shift ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# parameters

# data dir
if [[ "${data_dir}" ]]
then
    abs_data_dir=$(realpath ${data_dir})
    mkdir -p ${abs_data_dir}
else
    abs_data_dir=$(mktemp -d)
fi

# summary
(( verbose )) && echo "Options:"
(( verbose )) && echo "  verbose:      ${verbose}"
(( verbose )) && echo "  input:        ${input_media}"
(( verbose )) && echo "  mode:         ${mode}"
(( verbose )) && echo "  data_dir:     ${abs_data_dir}"
(( verbose )) && echo "  preserve:     ${preserve}"

function on_exit {   
    # variable to know whether we arrive here from an "exit 0" or "exit 1"
    rv=$?

    # do something before exiting (e.g. remove temporary files)
    if (( preserve != 1 ))
    then
	rm -rf ${abs_data_dir}
    fi
}

# when EXIT signal is sent to this script (either from an exit command or from a CTRL-C), go to finish function
trap on_exit EXIT

function analyse_media {
    cd ${abs_data_dir}
    
    case ${mode} in
	pts_size )
	    ffprobe -v quiet -show_entries frame=pkt_pts_time,pict_type,pkt_size -print_format compact=nk=1:p=0:s='\ ' -select_streams v:0 -i ${input_media} >fixed.dat && \
		sed -e 's/I/16711680/g' -e 's/P/65280/g' -e 's/B/255/g' fixed.dat >column.dat
	    ;;
 	cpn_size )
	    ffprobe -v quiet -show_entries frame=pkt_size,pict_type,coded_picture_number -print_format compact=nk=1:p=0:s='\ ' -select_streams v:0 -i ${input_media} >fixed.dat && \
		sed -e 's/I/16711680/g' -e 's/P/65280/g' -e 's/B/255/g' fixed.dat >column.dat
	    ;;
   	cpn_pts )
	    ffprobe -v quiet -show_entries frame=pkt_pts_time,coded_picture_number -print_format compact=nk=1:p=0:s='\ ' -select_streams v:0 -i ${input_media} >column.dat
	    ;;
	* )
	    echo "ERROR: Not implemented mode: ${mode}"
	    exit 1
    esac

}

function create_plot {
    cd ${abs_data_dir}
    plot_path=plot.txt

    case ${mode} in
	pts_size )
	    cat >${plot_path} <<EOF
# GNUPLOT "plot.txt"
set title '${input_media}'
set xlabel "pts time (s)"
set ylabel "size (bytes)"
#set yrange [0:120000]
#set ytics (120000, 110000, 100000, 90000, 80000, 70000, 60000, 50000, 40000, 30000, 20000, 10000)
#set xrange [-10:850]
set lmargin 12
set rmargin 2
set grid
set pointsize 2
set label 1 "I frames"
set label 1 at graph .85, .96 tc rgb "#ff0000"
set label 2 "P frames"
set label 2 at graph .85, .92 tc rgb "#00ff00"
set label 3 "B frames"
set label 3 at graph .85, .88 tc rgb "#0000ff"
# using x=1,y=2, with impulses, linecolor rgb variable (3)
plot 'column.dat' using 1:2:3 notitle with i lc rgb variable 	
EOF
	    
	    # 0xRRGGBB
	    # I: 0xff0000 (16711680) (red)
	    # P: 0x00ff00 (65280) (green)
	    # B: 0x0000ff (255) (blue)

	    gnuplot -p <plot.txt

	    ;;
	
	cpn_size )
	    cat >${plot_path} <<EOF
# GNUPLOT "plot.txt"
set title '${input_media}'
set xlabel "coded picture number"
set ylabel "size (bytes)"
#set yrange [0:120000]
#set ytics (120000, 110000, 100000, 90000, 80000, 70000, 60000, 50000, 40000, 30000, 20000, 10000)
#set xrange [-10:850]
set lmargin 12
set rmargin 2
set grid
set pointsize 2
set label 1 "I frames"
set label 1 at graph .85, .96 tc rgb "#ff0000"
set label 2 "P frames"
set label 2 at graph .85, .92 tc rgb "#00ff00"
set label 3 "B frames"
set label 3 at graph .85, .88 tc rgb "#0000ff"
# using x=1,y=2, with impulses, linecolor rgb variable (3)
plot 'column.dat' using 3:1:2 notitle with i lc rgb variable 	
EOF

	    # 0xRRGGBB
	    # I: 0xff0000 (16711680) (red)
	    # P: 0x00ff00 (65280) (green)
	    # B: 0x0000ff (255) (blue)
	    
	    gnuplot -p <plot.txt

	    ;;
	
	cpn_pts )
	    cat >${plot_path} <<EOF
# GNUPLOT "plot.txt"
set title '${input_media}'
set xlabel "coded picture number"
set ylabel "pts time (s)"
set grid
plot 'column.dat' using 2:1 notitle with lines
EOF
	    
	    gnuplot -p <plot.txt
	    
	    ;;
	
	* )
	    echo "ERROR: Not implemented mode: ${mode}"
	    exit 1
    esac
}		      

if [[ "${input_media}" ]]
then
    analyse_media
fi

sleep 2

abs_data_path=${abs_data_dir}/column.dat
if [ -s ${abs_data_path} ]
then
    create_plot
else
    echo "ERROR: ${abs_data_path} not found"
    exit 1
fi
    
exit 0
